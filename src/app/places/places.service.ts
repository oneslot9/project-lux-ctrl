import { Injectable } from '@angular/core';
import { Place } from './place.model'


@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  private places: Place[] = [ 
    {
      id: '1',
      title: 'Eiffel Tower',
      imageURL: 'https://th.bing.com/th/id/OIP.W9sOAIBEmy4hEPequ4XYlQHaMl?w=172&h=293&c=7&o=5&dpr=1.1&pid=1.7',
      comments: ['Amazing place', 'awesome experience']
    },
    {
      id: '2',
      title: 'Statue of Liberty',
      imageURL: 'https://th.bing.com/th/id/OIP.M3dR-WwPLir4ft5ppJyZhgHaNQ?w=172&h=309&c=7&o=5&dpr=1.1&pid=1.7',
      comments: ['Amazing place', 'awesome experience']
    },
    {
      id: '3',
      title: 'Awesome place',
      imageURL: 'https://th.bing.com/th/id/OIP.M3dR-WwPLir4ft5ppJyZhgHaNQ?w=172&h=309&c=7&o=5&dpr=1.1&pid=1.7',
      comments: []
    }
  ]

  constructor() { }

  getPlaces() {
    return [...this.places]
  }

  getPlace(placeId: string) {
    return {
      ...this.places.find(place => {
        return place.id ===placeId
      })
    }
  }

  addPlace(title: string, imageURL: string) {
    this.places.push({
      title,
      imageURL,
      comments: [],
      id: this.places.length + 1 + ""
  
  });
  }
    
  deletePlace(placeId: string) {
    this.places = this.places.filter(place => {
      return place.id !== placeId
    })
  }
}
 